//
//  main.m
//  test
//
//  Created by Алексей Шпирко on 01/03/14.
//  Copyright (c) 2014 AlexShpirko LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
