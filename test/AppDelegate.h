//
//  AppDelegate.h
//  test
//
//  Created by Алексей Шпирко on 01/03/14.
//  Copyright (c) 2014 AlexShpirko LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
