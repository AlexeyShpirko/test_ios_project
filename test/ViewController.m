//
//  ViewController.m
//  test
//
//  Created by Алексей Шпирко on 01/03/14.
//  Copyright (c) 2014 AlexShpirko LLC. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    NSMutableArray *testArray = [@[@1,@4,@3,@3,@1,@2,@4] mutableCopy];
    NSLog(@"%d", solution2(955));
    
    
}

int solution(NSMutableArray *array)
{
    __block NSInteger finalInt = -1;
    __block NSMutableArray *tmpArray = [NSMutableArray new];
    tmpArray = [array mutableCopy];

    
    [array enumerateObjectsUsingBlock:^(NSNumber *obj, NSUInteger idx, BOOL *stop){
        
        
            [tmpArray removeObject:obj];
            
            if ([array count] - [tmpArray count] == 1){
                finalInt = [obj integerValue];
                *stop = YES;
            }
            else {
                tmpArray = [array mutableCopy];
            }
        
    }];
     
    return finalInt;
}

int solution2(int N)
{
    
    NSMutableString *finalString = [NSMutableString stringWithFormat:@""];
    for(NSInteger i = N; i > 0; i >>= 1)
    {
        [finalString insertString:((i & 1) ? @"1" : @"0") atIndex:0];
    }
    
    
    
    
    return -1;
}



@end
